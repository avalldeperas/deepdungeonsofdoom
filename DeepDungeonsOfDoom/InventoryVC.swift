//
//  InventoryViewController.swift
//  DeepDungeonsOfDoom
//
//  Created by dmorenoar on 08/12/2018.
//  Copyright © 2018 avalldeperas. All rights reserved.
//

import UIKit

class InventoryVC: UIViewController {

    // images
    @IBOutlet weak var helmetImg: UIImageView!
    @IBOutlet weak var armourImg: UIImageView!
    @IBOutlet weak var bootsImg: UIImageView!
    @IBOutlet weak var weaponImg: UIImageView!
    @IBOutlet weak var shieldImg: UIImageView!
    @IBOutlet weak var accessoryImg: UIImageView!
    
    var heartImgVw:UIImageView!
    let tools:ToolsNew = ToolsNew()
    
    // labels
    @IBOutlet weak var lblAtq: UILabel!
    @IBOutlet weak var lblDef: UILabel!
    @IBOutlet weak var lblMag: UILabel!
    @IBOutlet weak var lblLck: UILabel!
    @IBOutlet weak var lblMoney: UILabel!
    @IBOutlet weak var lblExp: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    var images = [UIImageView]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        images = [helmetImg, armourImg, bootsImg, weaponImg, shieldImg, accessoryImg]
        
        setEquipImages()
        tools.setHearts(coord_x: 142, specie: iHero!, y: 638, w: 19, h: 19, imgPath: "heart", adding_x: 19, view: self)
        setStatsHero()
    }
    
    func setEquipImages() {
        for i in 0 ... images.count - 1 {
            images[i].image = iHero?.getEquip().getEquip()[equipKeys[i]]?.getImage()
        }
    }
    
    func setStatsHero(){
        lblAtq.text = String(iHero!.getEquip().totalAttack())
        lblDef.text = String(iHero!.getEquip().totalDefense())
        lblMag.text = String(iHero!.getEquip().totalMagic())
        lblLck.text = String(iHero!.getEquip().totalLuck())
        lblMoney.text = String(iHero!.getMoney())
        lblExp.text = String(iHero!.getExp())
        lblName.text = String(iHero!.getName())
    }
}
