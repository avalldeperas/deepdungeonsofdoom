//
//  Heroe.swift
//  DeepDungeonsOfDoom
//
//  Created by Albert Valldeperas on 14/11/2018.
//  Copyright © 2018 avalldeperas. All rights reserved.
//

import UIKit

class Monster: Specie {
    private var moneyLoot:Int
    private var exp:Int
    
    init (name:String, attack:Int, life:Int, moneyLoot:Int, exp:Int, image:UIImage){
        self.moneyLoot = moneyLoot
        self.exp = exp
        super.init(name: name, life: life, image: image, attack: attack)
    }
    
    public func getMoneyLoot() -> Int{
        return moneyLoot
    }
    
    public func getExp() -> Int{
        return exp
    }
    
    // method that will copy any monster
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = Monster(name: getName(), attack: getAttack(), life: getLife(), moneyLoot: moneyLoot, exp: exp, image: getImage())
        return copy
    }
}
