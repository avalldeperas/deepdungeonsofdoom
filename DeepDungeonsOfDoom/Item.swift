//
//  Heroe.swift
//  DeepDungeonsOfDoom
//
//  Created by Albert Valldeperas on 14/11/2018.
//  Copyright © 2018 avalldeperas. All rights reserved.
//

import UIKit

class Item {
    
    private var name:String
    private var attack:Int
    private var defense:Int
    private var magic:Int
    private var luck:Int
    private var money:Int
    private var image:UIImage
    private var type:String
    
    init (name:String, attack:Int, defense:Int, magic:Int, luck:Int, money:Int, image:UIImage, type:String){
        self.name = name
        self.attack = attack
        self.defense = defense
        self.magic = magic
        self.luck = luck
        self.money = money
        self.image = image
        self.type = type
    }
    
    public func getName() -> String{
        return name
    }
    
    public func getAttack() -> Int{
        return attack
    }
    
    public func getDefense() -> Int{
        return defense
    }
    
    public func getMagic() -> Int{
        return magic
    }
    
    public func getLuck() -> Int{
        return luck
    }
    
    public func getMoney() -> Int{
        return money
    }
   
    public func getImage() -> UIImage{
        return image
    }
    
    public func getType() -> String{
        return type
    }
}
