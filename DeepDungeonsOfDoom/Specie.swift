//
//  Specie.swift
//  DeepDungeonsOfDoom
//
//  Created by dmorenoar on 22/12/2018.
//  Copyright © 2018 avalldeperas. All rights reserved.
//

import Foundation
import UIKit

class Specie {
    
    private var name:String
    private var life:Int
    private var image:UIImage
    private var attack:Int
    
    init(name: String, life: Int, image: UIImage, attack:Int) {
        self.name = name
        self.life = life
        self.image = image
        self.attack = attack
    }
    
    public func getAttack() -> Int{
        return attack
    }
    
    public func getName() -> String{
        return name
    }
    
    public func getLife() -> Int{
        return life
    }
    
    public func setLife(life: Int) {
        if (life > 0) {
            self.life = life
        } else {
            self.life = 0
        }
    }
    
    public func getImage() -> UIImage{
        return image
    }
    
    
    
}
