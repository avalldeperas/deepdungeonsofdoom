//
//  Heroe.swift
//  DeepDungeonsOfDoom
//
//  Created by Albert Valldeperas on 14/11/2018.
//  Copyright © 2018 avalldeperas. All rights reserved.
//

import UIKit

class Equip {
    var equip = [String: Item]()
    
    init (equip:[String:Item]){
        self.equip = equip
    }
    
    public func getEquip() -> [String:Item]{
        return equip
    }
    
    public func totalAttack() -> Int {
        var totalAttack = 0
        for i in 0 ... equip.count - 1 {
            totalAttack += (equip[equipKeys[i]]?.getAttack())!
        }
        return totalAttack
    }
    
    public func totalDefense() -> Int {
        var totalDefense = 0
        for i in 0 ... equip.count - 1 {
            totalDefense += (equip[equipKeys[i]]?.getDefense())!
        }
        return totalDefense
    }
    
    public func totalMagic() -> Int {
        var totalMagic = 0
        for i in 0 ... equip.count - 1 {
            totalMagic += (equip[equipKeys[i]]?.getMagic())!
        }
        return totalMagic
    }
    
    public func totalLuck() -> Int {
        var totalLuck = 0
        for i in 0 ... equip.count - 1 {
            totalLuck += (equip[equipKeys[i]]?.getLuck())!
        }
        return totalLuck
    }
    
    
}
