//
//  File.swift
//  DeepDungeonsOfDoom
//
//  Created by dmorenoar on 22/12/2018.
//  Copyright © 2018 avalldeperas. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class ToolsNew {
    
    // Method that will show an alert showing info depending on the parameters passed from a certain viewcontroller.
    func showAlert(title: String, info: String, option: String, view: UIViewController){
        let alert = UIAlertController(title: title, message: info, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: option, style: .cancel, handler: nil))
        view.present(alert, animated: true)
    }
    
    // Method that will create Images views programatically. The number of images
    // will depend on the amount of life of the superclass 'Specie' that may be extended by Heroes or Monsters.
    func setHearts(coord_x: Int, specie: Specie, y: Int, w: Int, h: Int, imgPath: String, adding_x: Int, view: UIViewController) {
        var coord_x = coord_x
        for _ in 0 ..< specie.getLife() {
            let heartImgVw = UIImageView(frame: CGRect(x: coord_x, y: y, width: w, height: h))
            heartImgVw.image = UIImage(named: imgPath)
            heartImgVw.contentMode = .scaleAspectFit
            view.view.addSubview(heartImgVw)
            
            coord_x += adding_x
        }
    }
    
    // Method that enables the option to move from one viewcontroller to another
    func moveToViewController(context: String, to: String, view: UIViewController, animate: Bool){
        let vc = UIStoryboard(name: context, bundle: nil).instantiateViewController(withIdentifier: to)
        view.present(vc, animated: animate, completion: nil)
    }
    
    // method that creates and prepares a sound in order to be played whenever is needed.
    func createSound(src: String, type: String) -> AVAudioPlayer {
        var sound = AVAudioPlayer()
        do {
        sound = try AVAudioPlayer(contentsOf: URL.init(fileURLWithPath: Bundle.main.path(forResource: src, ofType: type)!))
            sound.prepareToPlay()
        } catch {
            print(error)
        }
        return sound
    }
}
