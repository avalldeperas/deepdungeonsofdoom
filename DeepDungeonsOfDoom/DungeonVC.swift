//
//  DungeonVC.swift
//  DeepDungeonsOfDoom
//
//  Created by Albert Valldeperas on 11/12/2018.
//  Copyright © 2018 avalldeperas. All rights reserved.
//

import UIKit
import AVFoundation

var monster:Monster?
var battleMonster:Monster?

class DungeonVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var monstersPV: UIPickerView!
    @IBOutlet weak var btnFight: UIButton!
    
    let tools:ToolsNew = ToolsNew()
    let labels = ["Name", "Attack", "Life", "Loot", "Exp"]
    
    @IBAction func btnFightClick(_ sender: UIButton) {
        if (battleMonster == nil){
            tools.showAlert(title: "Oops!", info: "You must choose a hero before start the battle!", option: "Ok", view: self)
        } else {
            let alert = UIAlertController(title: "You are about to battle to \(battleMonster!.getName())!", message: "Are you ready?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                self.tools.moveToViewController(context: "Main", to: "battle", view: self, animate: true)
            }))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            present(alert, animated: true)
        }
    }    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        monstersPV.dataSource = self
        monstersPV.delegate = self
        // default selection of row and monster.
        monstersPV.selectRow(0, inComponent: 0, animated: true)
        battleMonster = (monsters[0].copy() as! Monster)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return monsters.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var lblProperty:UILabel!
        let monster = monsters[row]
        var coord_y = -12
        
        let viewMonsters = UIView(frame: CGRect(x: 0, y: 0, width: 280, height: 150 ))
        let ivMonster = UIImageView(frame: CGRect(x: 0, y: 20, width: 100, height: 100))
        ivMonster.image = monster.getImage()
        viewMonsters.addSubview(ivMonster)
        
        for index in 0 ... labels.count - 1 {
            lblProperty = UILabel(frame: CGRect(x: 120, y: coord_y, width: 250, height: 100))
            lblProperty.text = labels[index] + ": " + getPropertyMonster(labels[index], monster: monster)
            lblProperty.textColor = UIColor.white
            lblProperty.font = UIFont(name: "Verdana", size: 15)
            viewMonsters.addSubview(lblProperty)
            coord_y += 18
        }
        
        return viewMonsters
    }
    
    /******** when the player selects a monster, a copy of it is generated *****/
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        battleMonster = (monsters[row].copy() as! Monster)
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 120.0
    }
    
    // method that gets a property of a monster depending on a string passed by the parameters.
    func getPropertyMonster(_ labelName: String, monster: Monster) -> String {
        var valueProperty:String?
       
        switch labelName {
            case "Name":
                valueProperty = monster.getName()
            case "Attack":
                valueProperty = String(monster.getAttack())
            case "Life":
                valueProperty = String(monster.getLife())
            case "Loot":
                valueProperty = String(monster.getMoneyLoot())
            case "Exp":
                valueProperty = String(monster.getExp())
            default:
                print("Error mate")
        }
        return valueProperty!
    }
}
