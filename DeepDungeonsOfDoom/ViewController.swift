//
//  ViewController.swift
//  DeepDungeonsOfDoom
//
//  Created by Albert Valldeperas on 14/11/2018.
//  Copyright © 2018 avalldeperas. All rights reserved.
//

import UIKit

// global variables
var items = [String:[Item]]()
var iHero:Hero?
let equipKeys = ["helmet", "armor", "boots","weapon", "shield", "accessory"]
var equipHeroes = [Equip]()
var monsters = [Monster]()
var itemsFull:[Item] = [Item]()


class ViewController: UIViewController {
    
    // heroe1
    @IBOutlet weak var imgHero1: UIImageView!
    @IBOutlet weak var nameHero1: UILabel!
    @IBOutlet weak var atqHero1: UILabel!
    @IBOutlet weak var defHero1: UILabel!
    @IBOutlet weak var magHero1: UILabel!
    @IBOutlet weak var lckHero1: UILabel!
    @IBOutlet weak var clickHero1: UIButton!
    
    //hero2
    @IBOutlet weak var imgHero2: UIImageView!
    @IBOutlet weak var nameHero2: UILabel!
    @IBOutlet weak var atqHero2: UILabel!
    @IBOutlet weak var defHero2: UILabel!
    @IBOutlet weak var magHero2: UILabel!
    @IBOutlet weak var lckHero2: UILabel!
    @IBOutlet weak var clickHero2: UIButton!
    
    //hero3
    @IBOutlet weak var imgHero3: UIImageView!
    @IBOutlet weak var nameHero3: UILabel!
    @IBOutlet weak var atqHero3: UILabel!
    @IBOutlet weak var defHero3: UILabel!
    @IBOutlet weak var magHero3: UILabel!
    @IBOutlet weak var lckHero3: UILabel!
    @IBOutlet weak var clickHero3: UIButton!
    
    // arrays
    var imagesHeroes = [UIImageView]()
    var namesHeroes = [UILabel]()
    var atqHeroes = [UILabel]()
    var defHeroes = [UILabel]()
    var magHeroes = [UILabel]()
    var lckHeroes = [UILabel]()
    
    // dictionaries
    var helmetItems = [Item]()
    var armorItems = [Item]()
    var bootsItems = [Item]()
    var weaponItems = [Item]()
    var shieldItems = [Item]()
    var accessoryItems = [Item]()
    
    var numItems = 6
    var numHeros = 3
    
    let heroImages = ["heroe1","heroe2","heroe3"]
    let heroNames = ["Arwen","Aureus","Kugan"]
    let monsterNames = ["Gollum", "TreeHound", "Rat","Scarab", "Spider", "Blue Lion", "Slime", ]
    
    var item:Item?
    var hero:Hero?
    var heroes = [Hero]()
    var equipsHeroes = [Equip]()
    
    @IBAction func chooseHeroClick(_ sender: UIButton) {
        iHero = heroes[sender.tag]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        imagesHeroes = [imgHero1, imgHero2, imgHero3]
        namesHeroes = [nameHero1, nameHero2, nameHero3]
        atqHeroes = [atqHero1, atqHero2, atqHero3]
        defHeroes = [defHero1, defHero2, defHero3]
        magHeroes = [magHero1, magHero2, magHero3]
        lckHeroes = [lckHero1, lckHero2, lckHero3]
        
        createItems()
        createEquips()
        createNewHeros()
        if(monsters.count == 0){
            createMonsters()
        }
    }
    
    // for each iteration a new hero will be created
    func createNewHeros(){
        for i in 0 ... numHeros - 1 {
            hero = Hero(name: heroNames[i], life: 5, money: 0, exp: 0, level: 0, equip:                  equipHeroes[i], image: UIImage(named: heroImages[i])!)
            heroes.append(hero!)
            imagesHeroes[i].image = hero!.getImage()
            namesHeroes[i].text = hero!.getName()
            atqHeroes[i].text = String(equipHeroes[i].totalAttack())
            defHeroes[i].text = String(equipHeroes[i].totalDefense())
            magHeroes[i].text = String(equipHeroes[i].totalMagic())
            lckHeroes[i].text = String(equipHeroes[i].totalLuck())
        }
    }
    
    // we create single hero equips without accessories and then append it to a list of equips
    // that will be used to attach them to each hero
    func createEquips() {
        var equipItems = [String:Item]()
        for _ in 0 ... numHeros - 1 {
            for i in 0 ... equipKeys.count - 2 {
                equipItems[equipKeys[i]] = items[equipKeys[i]]![0]
            }
            equipHeroes.append(Equip(equip: equipItems))
        }
    }
    
    // this function creates a database manually in which a list of items is created. each list of items is
    // separated by a type of item. This string 'type' will be used as KEY in the dictionary that will contain the
    // diferent list of items.
    func createItems(){
        armorItems.append(Item(name: "Leather Armor", attack: 0, defense: 4, magic: 0, luck: 0, money: 50, image: UIImage(named: "armor1")!, type: "armor"))
        armorItems.append(Item(name: "Plate Armor", attack: 0, defense: 10, magic: 0, luck: 0, money: 100, image: UIImage(named: "armor2")!, type: "armor"))
        armorItems.append(Item(name: "Magic Plate Armor", attack: 0, defense: 17, magic: 0, luck: 0, money: 200, image: UIImage(named: "armor3")!, type: "armor"))
        armorItems.append(Item(name: "Old Cape", attack: 0, defense: 2, magic: 1, luck: 1, money: 50, image: UIImage(named: "cape1")!, type: "armor"))
        armorItems.append(Item(name: "Focus Cape", attack: 0, defense: 9, magic: 2, luck: 2, money: 100, image: UIImage(named: "cape2")!, type: "armor"))
        armorItems.append(Item(name: "Dark Lord's Cape", attack: 0, defense: 11, magic: 8, luck: 5, money: 200, image: UIImage(named: "cape3")!, type: "armor"))
        bootsItems.append(Item(name: "Leather Boots", attack: 0, defense: 1, magic: 0, luck: 0, money: 50, image: UIImage(named: "boots1")!, type: "boots"))
        bootsItems.append(Item(name: "Guardian Boots", attack: 0, defense: 3, magic: 2, luck: 2, money: 150, image: UIImage(named: "boots2")!, type: "boots"))
        helmetItems.append(Item(name: "Viking Helmet", attack: 0, defense: 4, magic: 0, luck: 0, money: 50, image: UIImage(named: "helmet1")!, type: "helmet"))
        helmetItems.append(Item(name: "Soldier Helmet", attack: 0, defense: 7, magic: 0, luck: 0, money: 80, image: UIImage(named: "helmet2")!, type: "helmet"))
        helmetItems.append(Item(name: "Warrior Helmet", attack: 0, defense: 10, magic: 0, luck: 0, money: 120, image: UIImage(named: "helmet3")!, type: "helmet"))
        helmetItems.append(Item(name: "Death Helmet", attack: 0, defense: 15, magic: 0, luck: 2, money: 200, image: UIImage(named: "helmet4")!, type: "helmet"))
        helmetItems.append(Item(name: "Ferumbras Hat", attack: 0, defense: 1, magic: 0, luck: 0, money: 5000, image: UIImage(named: "helmet5_hat")!, type: "helmet"))
        accessoryItems.append(Item(name: "Crystal Necklace", attack: 0, defense: 2, magic: 1, luck: 2, money: 100, image: UIImage(named: "necklace")!, type: "accessory"))
        accessoryItems.append(Item(name: "Mana Potion", attack: 0, defense: 0, magic: 2, luck: 1, money: 40, image: UIImage(named: "potion1")!, type: "accessory"))
        accessoryItems.append(Item(name: "Great Mana Potion", attack: 0, defense: 0, magic: 6, luck: 3, money: 100, image: UIImage(named: "potion2")!, type: "accessory"))
        accessoryItems.append(Item(name: "Vampire's Ring", attack: 0, defense: 0, magic: 2, luck: 5, money: 100, image: UIImage(named: "ring1")!, type: "accessory"))
        accessoryItems.append(Item(name: "Death Ring", attack: 0, defense: 2, magic: 5, luck: 10, money: 200, image: UIImage(named: "ring2")!, type: "accessory"))
        weaponItems.append(Item(name: "Terra Wand", attack: 5, defense: 0, magic: 1, luck: 2, money: 50, image: UIImage(named: "wand1")!, type: "weapon"))
        weaponItems.append(Item(name: "Ice Wand", attack: 10, defense: 0, magic: 3, luck: 5, money: 100, image: UIImage(named: "wand2")!, type: "weapon"))
        weaponItems.append(Item(name: "Wand Of Plague", attack: 15, defense: 0, magic: 8, luck: 10, money: 200, image: UIImage(named: "wand3")!, type: "weapon"))
        weaponItems.append(Item(name: "Carlin Sword", attack: 5, defense: 0, magic: 0, luck: 0, money: 50, image: UIImage(named: "sword1")!, type: "weapon"))
        weaponItems.append(Item(name: "Long Sword", attack: 10, defense: 0, magic: 0, luck: 0, money: 80, image: UIImage(named: "sword2")!, type: "weapon"))
        weaponItems.append(Item(name: "Fire Sword", attack: 15, defense: 0, magic: 0, luck: 0, money: 150, image: UIImage(named: "sword3")!, type: "weapon"))
        weaponItems.append(Item(name: "Umbral Master Slayer", attack: 25, defense: 0, magic: 0, luck: 0, money: 250, image: UIImage(named: "sword4")!, type: "weapon"))
        weaponItems.append(Item(name: "Composite Bow", attack: 6, defense: 0, magic: 0, luck: 0, money: 50, image: UIImage(named: "bow1")!, type: "weapon"))
        weaponItems.append(Item(name: "Icicle Bow", attack: 12, defense: 0, magic: 0, luck: 1, money: 120, image: UIImage(named: "bow2")!, type: "weapon"))
        weaponItems.append(Item(name: "Umbral Master Bow", attack: 25, defense: 0, magic: 0, luck: 3, money: 250, image: UIImage(named: "bow3")!, type: "weapon"))
        shieldItems.append(Item(name: "Copper Shield", attack: 0, defense: 5, magic: 0, luck: 0, money: 40, image: UIImage(named: "shield1")!, type: "shield"))
        shieldItems.append(Item(name: "Steel Shield", attack: 0, defense: 10, magic: 0, luck: 0, money: 70, image: UIImage(named: "shield2")!, type: "shield"))
        shieldItems.append(Item(name: "Vampire Shield", attack: 0, defense: 15, magic: 0, luck: 0, money: 120, image: UIImage(named: "shield3")!, type: "shield"))
        shieldItems.append(Item(name: "Blessed Shield", attack: 0, defense: 30, magic: 0, luck: 3, money: 250, image: UIImage(named: "shield4")!, type: "shield"))
       
        insertItemsToMap(partItems: armorItems)
        insertItemsToMap(partItems: weaponItems)
        insertItemsToMap(partItems: helmetItems)
        insertItemsToMap(partItems: shieldItems)
        insertItemsToMap(partItems: accessoryItems)
        insertItemsToMap(partItems: bootsItems)
    }
    
    func insertItemsToMap(partItems: [Item]){
        items[partItems[0].getType()] = partItems
    }
    
    func createMonsters() {
        monsters.append(Monster(name: "Gollum", attack: 30, life: 9, moneyLoot: 150, exp: 50, image: UIImage(named: "monster1")!))
        monsters.append(Monster(name: "Tree Hound", attack: 30, life: 9, moneyLoot: 150, exp: 50, image: UIImage(named: "monster2")!))
        monsters.append(Monster(name: "Rat", attack: 5, life: 3, moneyLoot: 5, exp: 20, image: UIImage(named: "monster3")!))
        monsters.append(Monster(name: "Scarab", attack: 8, life: 6, moneyLoot: 25, exp: 50, image: UIImage(named: "monster4")!))
        monsters.append(Monster(name: "Spider", attack: 8, life: 4, moneyLoot: 8, exp: 30, image: UIImage(named: "monster5")!))
        monsters.append(Monster(name: "Rogue", attack: 15, life: 7, moneyLoot: 100, exp: 80, image: UIImage(named: "monster6")!))
        monsters.append(Monster(name: "Blue Lion", attack: 15, life: 7, moneyLoot: 150, exp: 50, image: UIImage(named: "monster7")!))
        monsters.append(Monster(name: "Slime", attack: 15, life: 5, moneyLoot: 150, exp: 50, image: UIImage(named: "monster8")!))
        monsters.append(Monster(name: "Rotting Frog", attack: 15, life: 4, moneyLoot: 150, exp: 50, image: UIImage(named: "monster9")!))
        monsters.append(Monster(name: "Scarab", attack: 7, life: 4, moneyLoot: 50, exp: 50, image: UIImage(named: "monster10")!))
        monsters.append(Monster(name: "Flying Artifact", attack: 15, life: 200, moneyLoot: 150, exp: 50, image: UIImage(named: "monster11")!))
        monsters.append(Monster(name: "Monk", attack: 15, life: 5, moneyLoot: 150, exp: 50, image: UIImage(named: "monster12")!))
        monsters.append(Monster(name: "Amazona", attack: 15, life: 4, moneyLoot: 150, exp: 50, image: UIImage(named: "monster13")!))
        monsters.append(Monster(name: "R2D3", attack: 15, life: 7, moneyLoot: 150, exp: 50, image: UIImage(named: "monster14")!))
        monsters.append(Monster(name: "Mad Dead King", attack: 10, life: 10, moneyLoot: 1500, exp: 500, image: UIImage(named: "monster15")!))
        monsters.sort(by: { $0.getLife() < $1.getLife() })
    }
}


