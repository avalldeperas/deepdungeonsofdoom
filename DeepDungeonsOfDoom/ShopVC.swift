//
//  ShopVC.swift
//  DeepDungeonsOfDoom
//
//  Created by avalldeperas on 09/12/2018.
//  Copyright © 2018 avalldeperas. All rights reserved.
//

import UIKit
import AVFoundation

class ShopVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var lblMoney: UILabel!
    @IBOutlet weak var shopPV: UIPickerView!
    @IBOutlet weak var keysPV: UIPickerView!
    @IBOutlet weak var btnBuy: UIButton!
    
    var sound = AVAudioPlayer()
    
    let messages = ["Name", "Attack", "Defense", "Magic", "Luck", "Price"]
    var actualKey = equipKeys[0]
    var itemToBuy:Item?
    let tools:ToolsNew = ToolsNew()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPickerViews()
        setHeroMoney()
        sound = tools.createSound(src: "cashRegister", type: ".mp3")
    }
    
    /* Method that sets all configuration of pickerviews */
    func setPickerViews(){
        keysPV.tag = 0
        shopPV.tag = 1
        shopPV.delegate = self
        shopPV.dataSource = self
        keysPV.delegate = self
        keysPV.dataSource = self
    }
    
    @IBAction func clickBtnBuy(_ sender: Any) {
        if (itemToBuy != nil){
            if (itemToBuy!.getMoney() > iHero!.getMoney()){
                tools.showAlert(title: "Oops!", info: "You don't have enough money to buy this item", option: "OK",view: self)
            } else {
                iHero!.getEquip().equip[actualKey] = itemToBuy
                iHero?.setMoney(money: iHero!.getMoney() - itemToBuy!.getMoney())
                setHeroMoney()
                sound.play()
                tools.showAlert(title: "Yay!", info: String("You just bougth " + itemToBuy!.getName()), option: "OK",view: self)
            }
        } else {
            tools.showAlert(title: "Oops!", info: "You need to choose an item before buying it.", option: "OK",view: self)
        }
    }
    
    func setHeroMoney() {
        lblMoney.text = String(iHero!.getMoney())
    }
    
    /* Method that depending on the labelName received will calculate diferent
     properties of a concrete item passed by the pickerview */
    func getPropertyItem(_ labelName: String, item: Item) -> String {
        var valueProperty:String?
        
        switch labelName {
        case "Name":
            valueProperty = item.getName()
        case "Attack":
            valueProperty = String(item.getAttack())
        case "Defense":
            valueProperty = String(item.getDefense())
        case "Luck":
            valueProperty = String(item.getLuck())
        case "Magic":
            valueProperty = String(item.getMagic())
        case "Price":
            valueProperty = String(item.getMoney())
        default:
            print("Error mate")
        }
        return valueProperty!
    }
    
    /************* PICKERVIEW METHODS ***************/
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if (pickerView.tag == 0){
            actualKey = equipKeys[row]
            shopPV.delegate = keysPV.delegate
        } else {
            itemToBuy = items[actualKey]![row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView.tag == 0){
            return equipKeys.count
        } else {
            return items[actualKey]!.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var myView = UIView ()
        var actualLabel = UILabel()
        var coord_y = -16
        var imageView = UIImageView()
        
        // top pickerview
        if (pickerView.tag == 0) {
            myView = UIView(frame: CGRect(x: 0, y: 0, width: 280, height: 90))
            actualLabel = UILabel(frame: CGRect(x: 100, y: 0, width: 250, height: 80))
            actualLabel.text = equipKeys[row]
            actualLabel.textColor = UIColor.white
            myView.addSubview(actualLabel)
            return myView
        // bottom pickerview
        } else {
            myView = UIView(frame: CGRect(x: 0, y: 0, width: 280, height: 150))
            imageView = UIImageView(frame: CGRect(x: 0, y: 20, width: 100, height: 100))
            imageView.image = items[actualKey]![row].getImage()
            myView.addSubview(imageView)
            
            // this iteration will set all labels of the second pickerview programatically
            for i in 0 ... messages.count - 1 {
                actualLabel = UILabel(frame: CGRect(x: 120, y: coord_y, width: 250, height: 100))
                actualLabel.text = messages[i] + ": " + getPropertyItem(messages[i], item: items[actualKey]![row])
                actualLabel.textColor = UIColor.white
                actualLabel.font = UIFont(name: "Verdana", size: 15)
                myView.addSubview(actualLabel)
                coord_y += 18
            }
        }
        return myView
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        if (pickerView.tag == 0){
            return 30.0
        } else {
            return 120.0
        }
    }
}

