//
//  BattleVC.swift
//  DeepDungeonsOfDoom
//
//  Created by Albert Valldeperas on 19/12/2018.
//  Copyright © 2018 avalldeperas. All rights reserved.
//

import UIKit
import AVFoundation

class BattleVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var heroPV: UIPickerView!
    @IBOutlet weak var monsterPV: UIPickerView!
    @IBOutlet weak var imgMonster: UIImageView!
    @IBOutlet weak var imgHero: UIImageView!
    @IBOutlet weak var lblAtqMonster: UILabel!
    @IBOutlet weak var lblAtqHero: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    
    var heroHearts = [UIImageView]()
    var monsterHearts = [UIImageView]()
    
    let tools:ToolsNew = ToolsNew()
    var victory = AVAudioPlayer()
    var defeat = AVAudioPlayer()
    var attack = AVAudioPlayer()
    var ouch = AVAudioPlayer()
    
    @IBOutlet weak var btnAttack: UIButton!
    
    @IBAction func clickBtnBack(_ sender: UIButton) {
        iHero!.setLife(life: 5)
    }
    
    func animateDices(pickerView: UIPickerView) -> Int {
        let duration: Double = 2
        let delay: Double = 0.2
        var atq:Int = 0
        for i in 0 ... pickerView.numberOfComponents - 1 {
            let rand = randomNumber(min: 1, max: 12)
            UIView.animate(withDuration: duration, delay: delay, usingSpringWithDamping: 0.6, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                pickerView.selectRow(rand, inComponent: i, animated: true)
            })
            atq += (rand + 1)
        }
        return atq
    }
    
    func animate(){
        
    }
    
    @IBAction func clickAttack(_ sender: UIButton) {
        
        let atqHero = animateDices(pickerView: heroPV)
        let atqMonster = animateDices(pickerView: monsterPV)
        
        lblAtqHero.text = String(atqHero)
        lblAtqMonster.text = String(atqMonster)
        
        battleResult(atqHero: atqHero, atqMonster: atqMonster)
    }
    
    func battleResult(atqHero: Int, atqMonster: Int){
        if (atqHero > atqMonster) {
            attack.play()
            removeHeart(specie: battleMonster!, &monsterHearts)
            if (battleMonster!.getLife() == 0) {
                btnAttack.isHidden = true
                iHero!.setMoney(money: iHero!.getMoney() +  battleMonster!.getMoneyLoot())
                iHero!.setExp(exp: iHero!.getExp() + battleMonster!.getExp())
                victory.play()
                btnBack.isHidden = false
                tools.showAlert(title: "YOU WON!", info: "You earned \(battleMonster!.getExp()) experience and \(battleMonster!.getMoneyLoot()) coins.", option: "OK", view: self)
            }
            
        } else if (atqHero < atqMonster) {
            ouch.play()
            removeHeart(specie: iHero!, &heroHearts)
            if (iHero!.getLife() == 0) {
                btnAttack.isHidden = true
                defeat.play()
                tools.showAlert(title: "Oops...", info: "You lost against the " + battleMonster!.getName() + ".", option: "OK", view: self)
                btnBack.isHidden = false
            }
        } else  {
            tools.showAlert(title: "Draw!", info: "You draw against your opponent.", option: "OK", view: self)
        }
    }
    
    func removeHeart(specie: Specie, _ hearts: inout [UIImageView]) {
        specie.setLife(life: specie.getLife() - 1)
        hearts[hearts.count - 1].removeFromSuperview()
        hearts.remove(at: hearts.count - 1)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnAttack.isHidden = false
        btnBack.isHidden = true
        setPickerViews()
        victory = tools.createSound(src: "victory", type: ".wav")
        defeat = tools.createSound(src: "defeat", type: ".wav")
        attack = tools.createSound(src: "attack", type: ".mp3")
        ouch = tools.createSound(src: "ouch", type: ".wav")
        heroHearts = setHearts(coord_x: 190, specie: iHero!, y: 620, w: 19, h: 19, imgPath: "heart", adding_x: 19, view: self)
        monsterHearts = setHearts(coord_x: 190, specie: battleMonster!, y: 100, w: 19, h: 19, imgPath: "heart", adding_x: 19, view: self)
    }
    
    func setHearts(coord_x: Int, specie: Specie, y: Int, w: Int, h: Int, imgPath: String, adding_x: Int, view: UIViewController) -> [UIImageView] {
        var coord_x = coord_x
        var hearts = [UIImageView]()
        for _ in 0 ..< specie.getLife() {
            let heartImgVw = UIImageView(frame: CGRect(x: coord_x, y: y, width: w, height: h))
            heartImgVw.image = UIImage(named: imgPath)
            heartImgVw.contentMode = .scaleAspectFit
            hearts.append(heartImgVw)
            view.view.addSubview(heartImgVw)
            coord_x += adding_x
        }
        return hearts
    }
    
    func setPickerViews(){
        monsterPV.tag = 0
        heroPV.tag = 1
        heroPV.delegate = self
        heroPV.dataSource = self
        monsterPV.delegate = self
        monsterPV.dataSource = self
        
        imgMonster.image = battleMonster?.getImage()
        imgHero.image = iHero?.getImage()
    }
    
    func calculateDiceNumber(specie: Specie) -> Int{
        let attack = specie.getAttack()
        if (attack <= 10) {
            return 1
        } else if (attack > 10 && attack <= 25) {
            return 2
        } else  {
            return 3
        }
    }
    
    func randomNumber(min: Int, max: Int) -> Int {
        return Int.random(in: min ... max)
    }
    
    /************ Pickerview methods **********/
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if (pickerView.tag == 0){
            return calculateDiceNumber(specie: battleMonster!)
        } else {
            return calculateDiceNumber(specie: iHero!)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 12
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
       
        var myView:UIView = UIView()
        var imageView:UIImageView = UIImageView()
        if (pickerView.tag == 0) {
            myView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
            imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
            imageView.image = UIImage(named: "dice\(row + 1)")
            myView.addSubview(imageView)
        } else {
            myView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
            imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
            imageView.image = UIImage(named: "dice\(row + 1)U")
            myView.addSubview(imageView)
        }
        return myView
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 150.0
    }
}
