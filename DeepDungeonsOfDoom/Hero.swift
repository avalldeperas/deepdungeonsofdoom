//
//  Heroe.swift
//  DeepDungeonsOfDoom
//
//  Created by Albert Valldeperas on 14/11/2018.
//  Copyright © 2018 avalldeperas. All rights reserved.
//

import UIKit

class Hero: Specie {
    private var money:Int
    private var exp:Int
    private var level:Int
    private var equip:Equip?
    
    init (name:String, life:Int, money:Int, exp:Int, level:Int, equip:Equip, image:UIImage){
        self.money = money
        self.exp = exp
        self.level = level
        self.equip = equip
        super.init(name: name, life: life, image: image, attack: equip.totalAttack())
    }
    
    public func getMoney() -> Int{
        return money
    }
    
    public func getExp() -> Int{
        return exp
    }
    
    public func setExp(exp: Int) {
        if (exp > 0) {
            self.exp = exp
        } else {
            self.exp = 0
        }
    }
    
    public func getEquip() -> Equip{
        return equip!
    }
    
    public func setMoney(money: Int) {
        self.money = money
    }
    
    public func getLevel() -> Int{
        return level
    }
    
    public func setLevel(level: Int) {
        self.level = level
    }
    
}

